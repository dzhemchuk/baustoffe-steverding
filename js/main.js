$(document).ready(function(){
	const companySlider = $(".company-slider");
	if(companySlider.length > 0){
		companySlider.owlCarousel({
			loop: true,
			items: 1,
			autoplay: false,
			mouseDrag: false,
			nav: false,
			margin: 0,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn'
		});
	
		$('.company-slider-arrow_prev').click(function() {
            companySlider.trigger('prev.owl.carousel');
		});
	
		$('.company-slider-arrow_next').click(function() {
			companySlider.trigger('next.owl.carousel');
		});
	}

	const offersSlider = $(".offers-slider");
	if(offersSlider.length > 0){
		offersSlider.owlCarousel({
			loop: true,
			autoplay: false,
			mouseDrag: false,
			nav: false,
			responsive: {
				0 : {
					items: 2,
					margin: 15
				},
				769 : {
					items: 2,
					margin: 15
				},
				993 : {
					items: 3,
					margin: 17
				}
			}
		});
	
		$('.offer-btn_prev').click(function() {
            offersSlider.trigger('prev.owl.carousel');
		});
	
		$('.offer-btn_next').click(function() {
			offersSlider.trigger('next.owl.carousel');
		});
	}

	const historyYears = $(".year-carousel");
	if(historyYears.length > 0){
		const setBtnYears = () => {
			$(".year-btn_prev .year-btn__title").text(parseInt($(".year-carousel .owl-item .year-item__year").eq($(".year-carousel .owl-item.active").first().index() - 1).text()));
			$(".year-btn_next .year-btn__title").text(parseInt($(".year-carousel .owl-item .year-item__year").eq($(".year-carousel .owl-item.active").last().index() + 1).text()));
		}

		historyYears.owlCarousel({
			loop: true,
			items: 3,
			autoplay: false,
			mouseDrag: false,
			nav: false,
			margin: 60,
			touchDrag: false,
			onRefreshed: setBtnYears,
			startPosition: parseInt($(".year-item").length / 2),
			responsive: {
				0 : {
					items: 1,
					margin: 15
				},
				769 : {
					items: 2,
					margin: 15
				},
				993 : {
					items: 3,
					margin: 30
				},
				1201 : {
					items: 3,
					margin: 60
				}
			}
		});
	
		$('.year-btn_prev').click(function() {
            historyYears.trigger('prev.owl.carousel');
			setBtnYears();
		});
	
		$('.year-btn_next').click(function() {
			historyYears.trigger('next.owl.carousel');
			setBtnYears();
		});
	}

	const historyPhotosSlider = $(".history-photos-slider");
	if(historyPhotosSlider.length > 0){
		historyPhotosSlider.owlCarousel({
			loop: true,
			items: 3,
			autoplay: true,
			mouseDrag: true,
			nav: false,
			margin: 30,
			autoWidth: true
		});
	}

	$(".request-select").click(function(){
		$(this).toggleClass("request-select_open");
	});

	$(".request-select-item").click(function(){
		$(this).parents(".request-select").find(".request-select__selected").text($(this).text());
		$(this).parents(".request-select").find("input[type='hidden']").val($(this).attr("data-value"));
	});

	$(".catalog-setting-item").click(function(){
		if($(this).children(".catalog-setting-item__input").prop("checked")){
			$(this).children(".catalog-setting-item__input").prop("checked", false);
		}
		else{
			$(this).children(".catalog-setting-item__input").prop("checked", true);
		}
	});

	$(".vacancy-title").click(function(){
		if($(this).siblings(".vacancy-body").is(":visible")){
			$(this).siblings(".vacancy-body").slideUp();
			$(this).parents(".vacancy").removeClass("vacancy_open");
		}
		else{
			$(this).siblings(".vacancy-body").slideDown({
				start: function () {
				  $(this).css({
					display: "flex"
				  })
				}
			});
			$(this).parents(".vacancy").addClass("vacancy_open");
		}
	});

	$(".header-nav__link").mouseover(function(){
		if($(window).width() >= 992){
			$(".header-hideble-list").hide();
		}
	});

	$(".header-hideble .header-nav__link").mouseover(function(){
		if($(window).width() >= 992){
			$(this).siblings(".header-hideble-list").css("display", "flex");
			let transform = (parseFloat($(this).siblings(".header-hideble-list").outerWidth()) - parseFloat($(this).outerWidth())) / 2;
			$(this).siblings(".header-hideble-list").css("transform", "translate(-"+ transform +"px, 0)");
		}
	});

	$(".header-hideble-list").mouseleave(function(){
		if($(window).width() >= 992){
			$(this).hide();
		}
	});

	$(".header-hideble .header-nav__link").click(function(e){
		if($(window).width() < 992){
			if(!$(this).siblings(".header-hideble-list").is(":visible")){
				$(this).siblings(".header-hideble-list").css("display", "flex");
				if($(window).width() > 768){
					e.preventDefault();
					let transform = (parseFloat($(this).siblings(".header-hideble-list").outerWidth()) - parseFloat($(this).outerWidth())) / 2;
					$(this).siblings(".header-hideble-list").css("transform", "translate(-"+ transform +"px, 0)");
				}
			}
		}
	});

	$(".header").mouseleave(function(){
		if($(window).width() >= 992){
			$(".header-hideble-list").hide();
		}
	});
	$(".request-checkbox__marker").click(function(){
		if($(this).siblings(".request-checkbox__input").prop("checked")){
			$(this).siblings(".request-checkbox__input").prop("checked", false);
		}
		else{
			$(this).siblings(".request-checkbox__input").prop("checked", true);
		}
	});
	$(".mobile-menu-open").click(function(){
		$(".header-nav").css("right", 0);
		$(".mobile-menu-bg").show();
		$("body").css("overflow", "hidden");
	});
	$(".mobile-menu-close").click(function(){
		$(".header-nav").css("right", "-" + $(".header-nav").outerWidth() + "px");
		$(".catalog-settings").css("right", "-" + $(".catalog-settings").outerWidth() + "px");
		$(".catalog-sorting").css("right", "-" + $(".catalog-settings").outerWidth() + "px");
		$(".mobile-menu-bg").hide();
		$("body").removeAttr('style');
	});
	$(".mobile-menu-bg").click(function(){
		$(".header-nav").css("right", "-" + $(".header-nav").outerWidth() + "px");
		$(".catalog-settings").css("right", "-" + $(".catalog-settings").outerWidth() + "px");
		$(".catalog-sorting").css("right", "-" + $(".catalog-settings").outerWidth() + "px");
		$(".mobile-menu-bg").hide();
		$("body").removeAttr('style');
	});

	// Catalog filter button
	$(".mobile-ctalog-buttons__btn_filter").click(function(){
		$(".catalog-settings").css("right", 0);
		$(".mobile-menu-bg").show();
		$("body").css("overflow", "hidden");
	});

	// Catalog sorting button
	$(".mobile-ctalog-buttons__btn_sorting").click(function(){
		$(".catalog-sorting").css("right", 0);
		$(".mobile-menu-bg").show();
		$("body").css("overflow", "hidden");
	});
});